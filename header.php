<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 16/09/14
 * Time: 09:14
 */ ?>
<div class="wrap b-gray" id="top">
    <div class="page row">
        <ul>
            <div class="col-lg-4 left">
                <a href="#topo" class="scroll">
                    <span>
                        <li><i class="fa fa-home"></i></li>
                    </span>
                </a>
            </div>
            <div class="col-lg-2 left">
                <a href="#missao" class="scroll">
                        <span>
                            <li><i class="fa fa-flag"></i> MISSÃO</li>
                        </span>
                </a>
            </div>
            <div class="col-lg-2 left">
                <a href="#equipe" class="scroll">
                    <span>
                        <li><i class="fa fa-users"></i> EQUIPE</li>
                    </span>
                </a>
            </div>
            <div class="col-lg-2 left">
                <a href="#localizacao" class="scroll">
                    <span>
                        <li><i class="fa fa-map-marker"></i> LOCALIZAÇÃO</li>
                    </span>
                </a>
            </div>
            <div class="col-lg-2 left">
                <a href="#contato" class="scroll">
                    <span>
                        <li><i class="fa fa-comments-o"></i> CONTATO</li>
                    </span>
                </a>
            </div>
        </ul>
    </div>
</div>
<div class="wrap b-blue" id="topo">
    <div class="page">
        <div class="header row">
            <div class="logo col-md-4">
                <a href="index.php">
                    <img class="logotipo" src="style/images/logo-1-m.png" alt="NETWAY - Desenvolvimento pra WEB"/>
                </a>
            </div>
            <div class="top-links col-md-8 row">
                <div class="col-md-12 right">
                    <div class="top-links">
                        <ul>
                            <a href="#missao" class="li-radius scroll">
                                        <span>
                                            <li><i class="fa fa-slideshare"></i> INSTITUCIONAL</li>
                                        </span>
                            </a>
                            <a href="#equipe" class="li-radius scroll">
                                        <span>
                                            <li><i class="fa fa-code"></i> SOLUÇÕES</li>
                                        </span>
                            </a>
                            <a href="#localizacao" class="li-radius scroll">
                                        <span>
                                            <li><i class="fa fa-laptop"></i> PORTIFOLHO</li>
                                        </span>
                            </a>
                            <a href="#contato" class="li-radius scroll">
                                        <span>
                                            <li><i class="fa fa-suitcase"></i> TRABALHE CONOSCO</li>
                                        </span>
                            </a>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>