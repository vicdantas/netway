<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 16/09/14
 * Time: 09:35
 */
?>
<div class="wrap b-gray" id="contato">
    <div class="page-full">
        <footer>
            <div class="row">
                <div class="col-md-3">
                    <div class="logo-footer">
                        <img src="style/images/logo-1-white.png" class="logo logo-footer"/>
                    </div>
                    <div class="adress">
                        <br/>
                        <adress>Av. Brig. Faria Lima, 000 - São Paulo - SP </adress>
                        <p>CEP: 00000-000</p>
                        <br />
                        <h4>Telefones pra contato:</h4>
                        <ul>
                            <li><i class="fa fa-phone"></i> 55 11 9521-76125</li>
                            <li><i class="fa fa-phone"></i> 55 11 9521-76125</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <h3>Institucional</h3>
                    <ul>
                        <li>Lorem Ipsum</li>
                        <li>Lorem Ipsum</li>
                        <li>Lorem Ipsum</li>
                        <li>Lorem Ipsum</li>
                    </ul>
                    <h3>Soluções</h3>
                    <ul>
                        <li>Lorem Ipsum</li>
                        <li>Lorem Ipsum</li>
                        <li>Lorem Ipsum</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3>Últimas Postagens</h3>
                    <a class="twitter-timeline" href="https://twitter.com/twitterapi" data-widget-id="507247990562488320">Tweets de @twitterapi</a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                </div>
                <div class="col-md-3 right">
                    <h4>Assine nossa newsletter!</h4>
                    <form role="form" class="newsletter" action="" method="post">

                        <div class="form-group">
                            <input type="email" class="form-control" id="NewsletterInputEmail" placeholder="Assinar Newsletter" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"/>
                            <button type="submit" class="btn btn-info" id="NewsletterSubmitEmail"><i class="fa fa-check-square-o"></i></button>
                        </div>
                    </form>
                    <br />
                    <br />
                    <br />
                    <h4>Siga-nos!</h4>
                    <div class="contato social">
                        <i class="fa fa-facebook-square"></i>
                        <i class="fa fa-linkedin-square"></i>
                        <i class="fa fa-twitter-square"></i>
                        <i class="fa fa-google-plus-square"></i>
                        <i class="fa fa-github-square"></i>
                        <i class="fa fa-git-square"></i>
                    </div>
                    <div id="curtir-plus">
                        <div class="g-plusone" data-size="tall" data-annotation="inline" data-width="300"></div>
                    </div>
                </div>
            </div>
            <div class="page row">
            </div>
        </footer>
    </div>
</div>