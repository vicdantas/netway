<!DOCTYPE html>
<html>
<head>
    <title>NETWAY - Desenvolvimento pra WEB</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="style/css/styles.css"/>
    <link rel="stylesheet" type="text/css" href="style/css/responsive.css"/>
    <link rel="stylesheet" href="style/font-awesome/css/font-awesome.min.css">
    <link href="style/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <link rel="shortcut icon" href="style/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="style/images/favicon.ico" type="image/x-icon">
     <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js'></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top}, 800);
            });
        });
    </script>
    <script src="https://apis.google.com/js/platform.js" async defer>
        {lang: 'pt-BR'}
    </script>
</head>
<body>
    <div class="wrapper">
        <?php include('elements/header.php');?>
        <div class="wrap b-photo">
        </div>
        <div class="wrap b-white" id="missao">
            <div class="page">
                <h2><i class="fa fa-flag"></i> MISSÃO</h2>
                <span class="">
                <i class="fa fa-quote-left"></i>
                    Planeja, desenvolver, criar, inovar e transformar ideias em projetos reais.
                    <i class="fa fa-quote-right"></i>
                </span>
                <span class="clear d-block"><a href="quem-somos.html">QUEM SOMOS</a> </span>
            </div>
        </div>
        <div class="wrap b-photo2">

        </div>
        <div class="wrap b-white-blue" id="equipe">
            <div class="page">
                <div class="team">
                    <h2 class="white"><i class="fa fa-users"></i> EQUIPE</h2>
                    <ul class="row">
                        <li class="left team col-md-12 first">
                            <div class="mask-circle border">
                                <a href="">
                                    <div class="cover-team two"></div>
                                </a>
                            </div>
                            <div class="desc left">
                                <h3 class="white"><i class="fa fa-user"></i> Victor Dantas</h3>
                                <span class="white">(CEO and CTO)</span>

                            </div>
                        </li>
                        <li class="left team col-md-12 second">
                            <a href="">
                                <div class="mask-circle border">
                                    <div class="cover-team two"></div>
                                </div>
                            </a>
                            <div class="desc left">
                                <h3 class="white"><i class="fa fa-user"></i> Bianca Laura </h3>
                                <span class="white">(CFO and CIO)</span>
                            </div>

                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wrap b-photo3">

        </div>
        <div class="wrap b-white" id="localizacao">
            <div class="page">
                <h2><i class="fa fa-map-marker"></i> LOCALIZAÇÃO</h2>
            </div>
            <div class="overlay" onClick="style.pointerEvents='none'"></div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.09818215328!2d-46.69427600000001!3d-23.564916999999973!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce57a148af42b9%3A0xcc4fddda281c72a1!2sFaria+Lima!5e0!3m2!1spt-BR!2sbr!4v1408423986970" width="100%" height="450" frameborder="0" style="border:0"></iframe>
        </div>
        <div class="wrap b-photo4">

        </div>
        <?php include ('elements/footer.php') ?>
    </div>
</body>
</html>